//
//  CustomWindow.swift
//  SwiftProject
//
//  Created by BENOIT PETITEAU on 14/12/2017.
//  Copyright © 2017 BENOIT PETITEAU. All rights reserved.
//

import UIKit

class CustomWindow: UIWindow {
    
    var wallpaper: UIImage? = UIImage(named: "background") {
        didSet {
            // refresh if the image changed
            setNeedsDisplay()
        }
    }
    
    init() {
        super.init(frame: UIScreen.main.bounds)
        //clear the background color of all table views, so we can see the background
        UITableView.appearance().backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func draw(_ rect: CGRect) {
        if let wallpaper = wallpaper {
            wallpaper.draw(in: self.bounds)
        } else {
            super.draw(rect)
        }
    }
//    override func draw(rect: CGRect) {
//        // draw the wallper if set, otherwise default behaviour
//        if let wallpaper = wallpaper {
//            wallpaper.drawInRect(self.bounds);
//        } else {
//            super.drawRect(rect)
//        }
//    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
