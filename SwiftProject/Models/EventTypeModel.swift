//
//  EventTypeModel.swift
//  SwiftProject
//
//  Created by BENOIT PETITEAU on 18/01/2018.
//  Copyright © 2018 BENOIT PETITEAU. All rights reserved.
//

import UIKit

/**
 * Modèle de l'objet EventType
 * Permettant de représenter la catégorie de l'évenement (administratif, concert, gastronomique ...)
 */
class EventTypeModel: NSObject {

    var id: Int?
    var label: String?
    
    // Affichage pour debug
    override var description: String { return "{id: \(String(describing: id)), label: \(String(describing: label))}"}

    override init() {
    }
    
    // initialisation à partir de JSON
    init(data: [String: Any]) {
        self.id = data["id"] as? Int
        self.label = data["label"] as? String
    }
    
}
