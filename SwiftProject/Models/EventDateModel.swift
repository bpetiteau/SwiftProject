//
//  EventDateModel.swift
//  SwiftProject
//
//  Created by BENOIT PETITEAU on 18/01/2018.
//  Copyright © 2018 BENOIT PETITEAU. All rights reserved.
//

import UIKit

/**
 * Modèle de l'objet EventDate
 * Représente une date complète (début & fin) associée à un event
 */
class EventDateModel: NSObject {

    var id: Int?
    var eventId : Int?
    var start: Date?
    var end: Date?
    var repetition: String?
    
    // Affichage pour debug
    override var description: String {
        return "{id: \(String(describing: id)), eventId: \(String(describing: eventId)), start: \(String(describing: start)), end: \(String(describing: end)), repetition: \(String(describing: repetition))}"
    }
    
    override init() {
    }
    
    // Initialisation à partir des données JSON
    init(data: [String: Any]) {
        self.id = data["id"] as? Int
        self.eventId = data["event_id"] as? Int
        self.repetition = data["repetition"] as? String
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss" // Format de date récupéré de la base de données
        if let start = data["start"] as? String {
            self.start = format.date(from: start)
        }
        if let end = data["end"] as? String {
            self.end = format.date(from: end)
        }
    }
}
