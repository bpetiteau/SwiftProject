//
//  EventModel.swift
//  SwiftProject
//
//  Created by BENOIT PETITEAU on 11/12/2017.
//  Copyright © 2017 BENOIT PETITEAU. All rights reserved.
//

import UIKit
import Foundation

/**
 * Modele de l'objet event
 */
class EventModel: NSObject {

    var id: Int? = nil
    var name: String? = nil
    var creatorId: Int? = nil
    var type: EventTypeModel? = nil
    var dates: [EventDateModel] = []
    var desc: String? = nil
    
    // Affichage pour debug
    override var description: String {
        return "{id: \(String(describing: id)), name: \(String(describing: name)), creatorId: \(String(describing: creatorId)), desc: \(String(describing: desc)), type: \(String(describing: type)), dates: \(dates)}"
    }
        
    override init() {
    }
    
    // initialisation de l'objet à partir des données récupérées en JSON
    init(data: [String: Any]) {
        self.id = data["id"] as? Int
        self.name = data["name"] as? String
        self.creatorId = data["creator_id"] as? Int
        self.desc = data["description"] as? String
        if let eventType = data["type"] as? [String: Any] {
            // initialisation d'un objet EventType
            self.type = EventTypeModel(data: eventType)
        }
        if let eventDates = data["dates"] as? [Any] {
            for eventDate in eventDates {
                if let eventDate = eventDate as? [String: Any] {
                    // initialisation d'un objet EventDate et ajout au tableau de dates
                    // L'objet event ainsi que la bdd ont été pensés pour stocker plusieurs dates
                    dates.append(EventDateModel(data: eventDate))
                }
            }
        }
    }
}
