//
//  LoginViewController.swift
//  SwiftProject
//
//  Created by BENOIT PETITEAU on 12/12/2017.
//  Copyright © 2017 BENOIT PETITEAU. All rights reserved.
//

import UIKit

/**
 * Vue présentant le formulaire de connection
 */
class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    let keychain = KeychainSwift()
    
    // Empêche les éléments de la vue de tourner avec le mobile
    override var shouldAutorotate : Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // utilisation de la méthode définie dans l'extension pour appliquer le fond d'écran :
        self.setBackgroundImage()
        loginTextField.delegate = self
        passwordTextField.delegate = self
        // Couleur et bordure arrondie des boutons :
        loginButton.layer.cornerRadius = 5
        loginButton.layer.borderColor = UIColor.white.cgColor
        loginButton.layer.borderWidth = 2
        registerButton.layer.cornerRadius = 5
        registerButton.layer.borderColor = UIColor.white.cgColor
        registerButton.layer.borderWidth = 2
    }
    
    // Cache la barre de navigation en arrivant sur la vue
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Passe directement à la vue principale si l'utilisateur est déjà loggé :
        segueIfLoggedIn()
    }
    
    // Remet visible la barre de navigation en quittant la vue
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    private func segueIfLoggedIn() {
        // Vérifie dans les UserDefaults si le booléen "logged" est à true, et si le token est bien rentré dans le keychain
        // Si c'est le cas, on passe à la vue principale en tant qu'utilisateur loggé
        if UserDefaults.standard.bool(forKey: "logged") && keychain.get("apiToken") != nil {
            self.performSegue(withIdentifier: "fromLoginToPrivateView", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Pour gérer le clavier des text fields :
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Action déclenchée à l'appui sur le bouton "connection"
    @IBAction func loginAction(_ sender: Any) {
        let login = loginTextField.text
        let password  = passwordTextField.text
        if(login == "" || password == "") {
            // Si un des deux champs n'est pas rempli, rien ne se passe
            return
        }
        // Sinon, on passe à la tentative de connection :
        apiRetrieveToken(username: login!, password: password!)
    }

    @IBAction func registerAction(_ sender: Any) {
    }
    
    // Méthode permettant d'interroger l'API pour récupérer un token d'authentification avec le login & le mdp fournis
    private func apiRetrieveToken(username: String, password: String) {
        // Création du corps de la requete en tableau, récupérant les données du fichier API (client secret, id) et le login et mdp
        let json: [String: Any] = ["grant_type": "password",
                                   "client_id": clientId,
                                   "client_secret": clientSecret,
                                   "username": username,
                                   "password": password]
        // sérialisation du tableau vers json
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // création de l'url à partir du domaine
        let url = URL(string: domain + "/oauth/token")!
        var request = URLRequest(url: url)
        // Méthode de la requête, corps et headers :
        request.httpMethod = "POST"
        request.httpBody = jsonData!
        request.setValue("application/json", forHTTPHeaderField: "Accept") // On souhaite avoir une réponse en JSON
        request.setValue("application/json", forHTTPHeaderField: "Content-Type") // On envoie une requête en JSON
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let response = response as! HTTPURLResponse
            guard let data = data, error == nil, response.statusCode == 200 else {
                // Si la requête retourne une réponse qui n'a pas le status 200 (par exemple, si le mdp n'est pas bon),
                // On affiche l'erreur et on arrête là
                DispatchQueue.main.async {self.errorLabel.text = "Login ou mot de passe incorrects"}
                return
            }
            DispatchQueue.main.async {self.errorLabel.text = ""}
            // Sérialisation de la réponse en JSON :
            let responseJSON = try! JSONSerialization.jsonObject(with: data, options: [])
            // De JSON à un tableau :
            if let dictionary = responseJSON as? [String: Any] {
                if let token = dictionary["access_token"] as? String {
                    // Récupération du champ access_token et sauvegarde de la valeur dans le keychain :
                    self.keychain.set(token, forKey: "apiToken")
                    // Récupération de l'utilisateur et connection :
                    DispatchQueue.main.async {self.apiRetrieveUser(token: token)}
                }
            }
        }
        task.resume()
    }
    
    // Méthode qui interroge l'Api pour récupérer l'ID de l'utlisateur connecté
    private func apiRetrieveUser(token: String) {
        // Création de la requête
        let url = URL(string: domain + "/api/userinfo")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        // Utilisation du token d'authorisation récupéré précedemment :
        request.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let response = response as! HTTPURLResponse
            guard let data = data, error == nil, response.statusCode == 200 else {
                // Si la requête retourne une réponse qui n'a pas le status 200 (par exemple, si le mdp n'est pas bon),
                // On affiche l'erreur et on arrête là
                DispatchQueue.main.async {self.errorLabel.text = "Login ou mot de passe incorrects"}
                return
            }
            DispatchQueue.main.async {self.errorLabel.text = ""}
            // Sérialisation de la réponse en JSON :
            let responseJSON = try! JSONSerialization.jsonObject(with: data, options: [])
            if let dictionary = responseJSON as? [String: Any] {
                // récupération de l'ID de l'utilisateur actuel
                if let userId = dictionary["id"] as? Int {
                    // Stockage de l'ID dans les UserDefaults
                    UserDefaults.standard.set(userId, forKey: "userId")
                    // Champs "logged" des UserDefaults mis à true pour signifier que l'utilisateur est bien connecté
                    UserDefaults.standard.set(true, forKey: "logged")
                    // On passe à la vue principale :
                    DispatchQueue.main.async {self.segueIfLoggedIn()}
                }
            }
        }
        task.resume()
    }
    
}
