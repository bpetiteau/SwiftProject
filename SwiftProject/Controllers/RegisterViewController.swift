//
//  RegisterViewController.swift
//  SwiftProject
//
//  Created by BENOIT PETITEAU on 13/12/2017.
//  Copyright © 2017 BENOIT PETITEAU. All rights reserved.
//

import UIKit

/**
 * Vue présentant le formulaire d'inscription
 */
class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    // Empêche les éléments de la vue de tourner avec le mobile
    override var shouldAutorotate : Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // utilisation de la méthode définie dans l'extension pour appliquer le fond d'écran :
        self.setBackgroundImage()
        nameTextField.delegate = self
        loginTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        repeatPasswordTextField.delegate = self
        // Bordure et couleur des boutons : 
        registerButton.layer.cornerRadius = 5
        registerButton.layer.borderColor = UIColor.white.cgColor
        registerButton.layer.borderWidth = 2
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Cache la barre de navigation en arrivant sur la vue
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // Remet visible la barre de navigation en quittant la vue
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    /**
     * TextField delegate for return key handling.
     * Can be done in storyboard instead under "auto-enable return key"
     **/
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    // Action apellée en appuyant sur le bouton "inscription"
    @IBAction func registerAction(_ sender: Any) {
        let name = nameTextField.text
        let login = loginTextField.text
        let email = emailTextField.text
        let password = passwordTextField.text
        let repeatPassword = repeatPasswordTextField.text
        if name == "" || login == "" || email == "" || password == "" || repeatPassword == "" {
            // Prévient l'utilisateur si les champs ne sont pas tous remplis, et ne fait rien d'autre
            errorLabel.text = "Tous les champs doivent être remplis"
            return
        }
        if password != repeatPassword {
            // Prévient l'utilisateur si le mot de passe répété ne correspond pas, et ne fait rien d'autre
            errorLabel.text = "Les mots de passe ne correspondent pas"
            return
        }
        errorLabel.text = ""
        // Passe à l'inscription avec l'API
        apiRegister(name: name!, login: login!, email: email!, password: password!)
    }
    
    // Méthode interogeant l'API pour inscrire un utilisateur dans la BDD
    private func apiRegister(name: String, login: String, email: String, password: String) {
        // Préparation des données à envoyer
        let json: [String: Any] = ["name": name,
                                   "login": login,
                                   "email": email,
                                   "password": password]
        // Sérialisation en JSON
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // Création de la requête (url, méthode, headers)
        let url = URL(string: domain + "/api/users")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData!
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let response = response as! HTTPURLResponse
            guard error == nil, response.statusCode == 201 else {
                // Si le code de reponse n'est pas 201, affichage d'une erreur
                // A l'avenir, les différents codes d'erreur devront être gérés independemment
                DispatchQueue.main.async {self.errorLabel.text = "Utilisateur deja existant"}
                return
            }
            // Si pas d'erreurs, tout va bien, on revient sur la page de connection :
            DispatchQueue.main.async {self.performSegue(withIdentifier: "fromRegisterToLogin", sender: self)}
        }
        task.resume()
    }
}
