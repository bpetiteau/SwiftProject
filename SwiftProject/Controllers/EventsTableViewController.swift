//
//  EventsTableViewController.swift
//  SwiftProject
//
//  Created by BENOIT PETITEAU on 15/01/2018.
//  Copyright © 2018 BENOIT PETITEAU. All rights reserved.
//

import UIKit

/**
 * Vue présentant la liste des évenements de l'utilisateur connecté
 */
class EventsTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var disconnectButon: UIButton!
    
    let keychain = KeychainSwift()
    var token: String?
    var userId: Int?
    var selectedEvent: EventModel?
    
    // Liste des évenements
    var events: [EventModel] = []
    // Liste des sections de la table view
    // Dans notre cas, une section correspond à un jour, les évenements du même jour seront dans la même section
    var tableSections: [String] = [""]
    
    // Identifiant d'une cellule de la table view
    let textCellIdentifier = "eventCell"

    @IBOutlet weak var eventTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // utilisation de la méthode définie dans l'extension pour appliquer le fond d'écran :
        self.setBackgroundImage()
        // Couleur et bordure des boutons :
        self.disconnectButon.layer.cornerRadius = 5
        self.disconnectButon.layer.borderColor = UIColor.white.cgColor
        self.disconnectButon.layer.borderWidth = 2
        self.eventTableView.delegate = self
        self.eventTableView.dataSource = self
        // Récupération du token d'uathentification ainsi que de l'ID de l'utilisateur :
        self.token = keychain.get("apiToken")
        self.userId = UserDefaults.standard.integer(forKey: "userId")
        // Appel à l'API pour récupérer la liste des évenements de l'utilisateur connecté :
        self.apiRetrieveEvents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func logoutAction(_ sender: Any) {
        // En cas de déconnection, on vide le keychain et on passe le champ "logged" des UserDefaults à false
        UserDefaults.standard.set(false, forKey: "logged")
        UserDefaults.standard.removeObject(forKey: "userId")
        keychain.delete("apiToken")
        // Et on redirige vers la page de connection
        self.performSegue(withIdentifier: "logoutSegue", sender: sender)
    }
    
    // Méthode renvoyant le nombre de lignes dans une section de la tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getSectionItems(section: section).count
    }
    
    // Méthode renvoyant une ligne de la tableView à partir d'un index
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as UITableViewCell
        // récupération des items dans la section
        let sectionItems = self.getSectionItems(section: indexPath.section)
        
        cell.textLabel?.text = sectionItems[indexPath.row].name ?? "Sans nom"
        
        return cell
    }
    
    // Méthode renvoyant le nombre de sections dans la table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableSections.count
    }
    
    // Méthode renvoyant le nom de la section courante
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tableSections[section]
    }
    
    // Méthode enregistrant l'evenement sur lequel on appuie
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionItems = self.getSectionItems(section: indexPath.section)
        selectedEvent = sectionItems[indexPath.row]
        // Et on passe à la vue de détails de l'évenement en question
        self.performSegue(withIdentifier: "detailSegue", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let eventDetailViewController  = segue.destination as? EventDetailViewController {
            // Si la vue suivante est une vue de détail d'event, on lui passe l'event sélectionné
            eventDetailViewController.selectedEvent = selectedEvent
        }
    }
    
    // Récupération de tous les items d'une section
    private func getSectionItems(section: Int) -> [EventModel] {
        var sectionItems = [EventModel]()
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short // Pour ne prendre en compte que la date, pas l'heure
        dateFormatter.locale = Locale(identifier: "FR-fr")
        // parcours des evenements pour garder ceux dont la date correspond
        for event in events {
            let eventDate = dateFormatter.string(from: event.dates[0].start!)
            // si la date de l'event correspond à la date de la section, on l'ajoute au tableau
            if eventDate == tableSections[section] as String {
                sectionItems.append(event)
            }
        }
        
        return sectionItems
    }
    
    // Appel à L'API pour récupérer tous les events de l'utilisateur
    private func apiRetrieveEvents() {
        // Préparation de la requête
        // Url composé avec l'id de l'utilisateur :
        let url = URL(string: domain + "/api/users/" + String(userId!) + "/events")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        // Utilisation du token d'authorisation :
        request.setValue("Bearer " + token!, forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let response = response as! HTTPURLResponse
            guard let data = data, error == nil, response.statusCode == 200 else {
                // En cas d'erreur, on ne continue pas
                print("TODO: handle http errors")
                return
            }
            // Sérialisation de la réponse en JSON
            let responseJSON = try! JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [Any] {
                for event in responseJSON {
                    if let event = event as? [String: Any] {
                        // Création d'un nouvel objet event à partir des données JSON et ajout de cet objet au tableau d'events
                        self.events.append(EventModel(data: event))
                    }
                }
            }
            DispatchQueue.main.async {
                // Remplissage de la table view avec nos events fraichement récupérés
                self.fillTableView()
            }
        }
        task.resume()
    }
    
    private func fillTableView() {
        // Tri du tableau d'events selon leur première date de début (dates[0].start) :
        self.events = self.events.sorted(by: {$0.dates[0].start!.compare($1.dates[0].start!) == .orderedAscending})
        // On nettoie les sections et on recommence :
        tableSections.removeAll()
        if events.count > 0 {
            // Repérage des différentes sections
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short // format de la date 10/01/2018
            dateFormatter.locale = Locale(identifier: "FR-fr")
            var currentDate = events[0].dates[0].start!
            tableSections.append(dateFormatter.string(from: currentDate))
            for event in events {
                // Comparaison de la nouvelle date avec la précédente en ne prenant en compte que le jour, par l'heure
                if Calendar.current.compare(currentDate, to: event.dates[0].start!, toGranularity: .day) != .orderedSame {
                    // Si la nouvelle date est différente de la date précédente, on en fait un section
                    currentDate = event.dates[0].start!
                    tableSections.append(dateFormatter.string(from: currentDate))
                }
            }
        }
        // On recharge la table view avec ces nouvelles données
        self.eventTableView.reloadData()
    }
}
