//
//  EventDetailViewController.swift
//  SwiftProject
//
//  Created by BENOIT PETITEAU on 22/01/2018.
//  Copyright © 2018 BENOIT PETITEAU. All rights reserved.
//

import UIKit

/**
 * Vue présentant les détails d'un event spécifique (nom, dates, description)
 */
class EventDetailViewController: UIViewController {

    var selectedEvent: EventModel? = nil
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // utilisation de la méthode définie dans l'extension pour appliquer le fond d'écran :
        super.setBackgroundImage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Affichage des caractéristiques de l'event récupéré dans les différents champs de la vue
        self.nameLabel.text = selectedEvent!.name
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium // format de date
        dateFormatter.timeStyle = .medium // format d'heure
        dateFormatter.locale = Locale(identifier: "FR-fr")
        var date = dateFormatter.string(from: selectedEvent!.dates[0].start!)
        if let end = selectedEvent!.dates[0].end {
            // Si la date possède une date de fin, on l'affiche à la suite d'une flèche
            date = date + " -> " + dateFormatter.string(from: end)
        }
        self.dateLabel.text = date
        self.descriptionTextView.text = selectedEvent!.desc
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
