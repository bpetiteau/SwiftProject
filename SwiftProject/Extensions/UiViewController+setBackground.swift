//
//  UiViewController+setBackground.swift
//  SwiftProject
//
//  Created by BENOIT PETITEAU on 08/01/2018.
//  Copyright © 2018 BENOIT PETITEAU. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    // Extension de UIViewController, pour simplifier l'application du fond d'écran sur toutes les vues concernées
    func setBackgroundImage() {
        let imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.image = UIImage(named: "background")
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
}
